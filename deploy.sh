
# fail script if a command fails
set -e
cd /var/www/node-test-1

#remove local changes
git reset --hard
git clean -fd

#reset to this build commit (can also be a rollback)
git pull origin	developement
git reset --hard $CI_COMMIT_SHA

npm update
node src &