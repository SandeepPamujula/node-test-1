var restify = require('restify');
 
const server = restify.createServer({
  name: 'myapp',
  version: '2.0.0'
});
 
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
 
server.get('/echo/:name', function (req, res, next) {
  res.send(req.params);
  return next();
});
 
server.listen(4000, function () {
  console.log('%s aws listening at %s', server.name, server.url);
});

